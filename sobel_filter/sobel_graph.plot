set terminal pngcairo size 410,250 enhanced font 'Verdana,9'

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12

set xtics rotate by -45
set ylabel 'msec'



set style data histograms
set size square
set boxwidth 1 relative
set style fill solid 1.0 border -1
set nokey

set output "beach_comp.png";
plot "beach.jpg.dat" using 2:xticlabels(1)

set output "earth_comp.png";
plot "earth.jpg.dat" using 2:xticlabels(1)

set output "hs_comp.png";
plot "hs.jpg.dat" using 2:xticlabels(1)

set output "parrot_comp.png";
plot "parrot.jpg.dat" using 2:xticlabels(1)

