#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include "../timing.h"

#ifdef ISPC_USE_HPX
	#include <hpx/hpx_main.hpp>
#endif

#include "objs/sobel_ispc_tasks.h"



using namespace cv;
using namespace std;

extern void sobel_serial(unsigned char* input, unsigned char* output, int rows, int cols);


void print_data(unsigned char* input, int rows, int cols){
	for(int i = 0; i < rows; i++){
		for(int j = 0;j < cols; j++){
			cout << (unsigned short)input[i*cols+j] << " ";
		}
		cout << endl;
	}
}



int main( int argc, char** argv ) {
	if( argc < 3) {
		cout <<" Usage: " << argv[0] << " input_image output_image" << endl;
		return -1;
	}

	Mat image;
	image = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);

	if(! image.data ) {
		cout <<  "Could not open or find the image" << std::endl ;
		return -1;
	}

	unsigned char* input = image.data;
	int rows = image.rows;
	int cols = image.cols;

	unsigned char* output = (unsigned char*) malloc(rows*cols*sizeof(unsigned char));

	vector<double> dt;
	for(int i = 0; i < 10; i++){
		reset_and_start_timer();
		ispc::sobel_ispc_tasks(input, output, rows, cols);
		dt.push_back(get_elapsed_msec());
	}
	double sum = std::accumulate(dt.begin(), dt.end(), 0.0);
	double mean = sum / dt.size();
	double median;
	sort(dt.begin(), dt.end());
	if (dt.size()%2 == 0) median = (dt[dt.size()/2-1] + dt[dt.size()/2]) / 2;
	else median = dt[dt.size()/2];

	cout << median <<endl;

	Mat out(rows, cols, CV_8UC1, output);

	char *filename = argv[2];
	imwrite(filename, out );

	return 0;
}
