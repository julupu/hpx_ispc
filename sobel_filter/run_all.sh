#!/bin/sh
images="images/beach.jpg images/earth.jpg images/hs.jpg images/parrot.jpg"

mkdir -p images_out
for i in $images; do
	echo $i
	echo "#$i" > $(basename $i).dat
	echo -e -n "hpx\t" >> $(basename $i).dat && ./sobel_ispc_tasks_hpx $i images_out/$(basename $i)_out_hpx.png --hpx:threads=all >> $(basename $i).dat
	echo -e -n "hpxv1\t" >> $(basename $i).dat && ./sobel_ispc_tasks_hpx_v1 $i images_out/$(basename $i)_out_hpxv1.png --hpx:threads=all >> $(basename $i).dat
	echo -e -n "omp\t" >> $(basename $i).dat && ./sobel_ispc_tasks_omp $i images_out/$(basename $i)_out_omp.png >> $(basename $i).dat
	echo -e -n "pthreads\t" >> $(basename $i).dat && ./sobel_ispc_tasks_pthreads $i images_out/$(basename $i)_out_pthreads.png >> $(basename $i).dat
done

