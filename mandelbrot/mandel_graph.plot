set terminal pngcairo size 410,250 enhanced font 'Verdana,9'

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12

set key outside right

set xtics rotate by -45
set ylabel 'msec'
set xlabel 'scale'



set style function lines
show style function


set output "comp_mandelbrot.png";
plot "mandel_hpxv1_comp.data" using 1:2 with linespoints title 'hpxv1', "mandel_hpx_comp.data" using 1:2 with linespoints title 'hpx',"mandel_omp_comp.data" using 1:2 with linespoints title 'omp', "mandel_pthreads_comp.data" using 1:2 with linespoints title 'pthreads'
