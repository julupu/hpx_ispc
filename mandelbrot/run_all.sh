#!/bin/sh
> mandel_hpx_comp.data
> mandel_hpxv1_comp.data
> mandel_omp_comp.data
> mandel_pthreads_comp.data

for i in $(seq 1 1 10); do 
		echo -n $i " " >> mandel_hpx_comp.data && ./mandelbrot_tasks_hpx --scale=$i --hpx:threads=all >> mandel_hpx_comp.data
		mv mandelbrot-ispc.ppm mandelbrot-ispc-hpx.ppm
		echo -n $i " " >> mandel_hpxv1_comp.data && ./mandelbrot_tasks_hpx_v1 --scale=$i --hpx:threads=all >>  mandel_hpxv1_comp.data
		mv mandelbrot-ispc.ppm mandelbrot-ispc-hpxv1.ppm
		echo -n $i " " >> mandel_omp_comp.data && ./mandelbrot_tasks_omp --scale=$i >> mandel_omp_comp.data
		mv mandelbrot-ispc.ppm mandelbrot-ispc-omp.ppm
		echo -n $i " " >> mandel_pthreads_comp.data && ./mandelbrot_tasks_pthreads --scale=$i >> mandel_pthreads_comp.data
		mv mandelbrot-ispc.ppm mandelbrot-ispc-pthreads.ppm
done
