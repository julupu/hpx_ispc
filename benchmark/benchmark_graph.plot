set terminal pngcairo size 410,250 enhanced font 'Verdana,9'

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12

set key outside right

set xtics rotate by -45
set ylabel 'msec'



set style function lines
show style function


set output "comp_hpx_tasks.png";
set xlabel 'task count'
plot "hpx_comp_tasks.data" using 1:2 with linespoints title 'hpx', "hpxv1_comp_tasks.data" using 1:2 with linespoints title 'hpxv1', "omp_comp_tasks.data" using 1:2 with linespoints title 'omp', "pthreads_comp_tasks.data" using 1:2 with linespoints title 'pthreads'

set output "comp_hpx_work.png";
set xlabel 'task size'
plot "hpx_comp_work.data" using 1:2 with linespoints title 'hpx', "hpxv1_comp_work.data" using 1:2 with linespoints title 'hpxv1', "omp_comp_work.data" using 1:2 with linespoints title 'omp', "pthreads_comp_work.data" using 1:2 with linespoints title 'pthreads'

set output "comp_hpx.png";
set xlabel 'task count and size'
plot "hpx_comp.data" using 1:2 with linespoints title 'hpx', "hpxv1_comp.data" using 1:2 with linespoints title 'hpxv1', "omp_comp.data" using 1:2 with linespoints title 'omp', "pthreads_comp.data" using 1:2 with linespoints title 'pthreads'
