#!/bin/sh

> hpx_comp_tasks.data
> hpxv1_comp_tasks.data
> omp_comp_tasks.data
> pthreads_comp_tasks.data

> hpx_comp_work.data
> hpxv1_comp_work.data
> omp_comp_work.data
> pthreads_comp_work.data

> hpx_comp.data
> hpxv1_comp.data
> omp_comp.data
> pthreads_comp.data


# par 1: task count
# par 2: task size

for i in $(seq 10000 10000 100000); do 
		echo -n $i " " >> hpx_comp_tasks.data && ./bench_hpx $i 1000 10 --hpx:threads=all >> hpx_comp_tasks.data
		echo -n $i " " >> hpxv1_comp_tasks.data && ./bench_hpxv1 $i 1000 10 --hpx:threads=all  >> hpxv1_comp_tasks.data
		echo -n $i " " >> omp_comp_tasks.data && ./bench_omp $i 1000 10 >> omp_comp_tasks.data
		echo -n $i " " >> pthreads_comp_tasks.data && ./bench_pthreads $i 1000 10 >> pthreads_comp_tasks.data

		echo -n $i " " >> hpx_comp_work.data && ./bench_hpx 1000 $i 10 --hpx:threads=all >> hpx_comp_work.data
		echo -n $i " " >> hpxv1_comp_work.data && ./bench_hpxv1 1000 $i 10 --hpx:threads=all >> hpxv1_comp_work.data
		echo -n $i " " >> omp_comp_work.data && ./bench_omp 1000 $i 10 >> omp_comp_work.data
		echo -n $i " " >> pthreads_comp_work.data && ./bench_pthreads 1000 $i 10 >> pthreads_comp_work.data
done

for i in $(seq 10000 10000 50000); do 
		echo -n $i " " >> hpx_comp.data && ./bench_hpx $i $i 10 --hpx:threads=all >> hpx_comp.data
		echo -n $i " " >> hpxv1_comp.data && ./bench_hpxv1 $i $i 10 --hpx:threads=all >> hpxv1_comp.data
		echo -n $i " " >> omp_comp.data && ./bench_omp $i $i 10 >> omp_comp.data
		echo -n $i " " >> pthreads_comp.data && ./bench_pthreads $i $i 10 >> pthreads_comp.data
done
