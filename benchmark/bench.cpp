#include "../timing.h"
#include "objs/bench.h"
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <iostream>

#ifdef ISPC_USE_HPX
        #include <hpx/hpx_main.hpp>
#endif

using namespace std;

extern void ispcfunc(int tasks, int worksize);

int main(int argc, char** argv){
	int tasks = atoi(argv[1]);
	int worksize = atoi(argv[2]);
	int testruns = atoi(argv[3]);
	double minISPC = 1e30;
	vector<double> dt;
	for(int i = 0; i < testruns; i++){
		reset_and_start_timer();
		ispc::ispcfunc(tasks, worksize);
		dt.push_back(get_elapsed_msec());
		minISPC = std::min(minISPC, dt[i]);
	}

	double sum = std::accumulate(dt.begin(), dt.end(), 0.0);
	double mean = sum / dt.size();
	double median;
	sort(dt.begin(), dt.end());
	if (dt.size()%2 == 0) median = (dt[dt.size()/2-1] + dt[dt.size()/2]) / 2;
	else median = dt[dt.size()/2];

	cout << median <<endl;
}

